import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor() {
  }

  getAll(){
    return axios.get(environment.apiUrl + "/posts");
  }
}
