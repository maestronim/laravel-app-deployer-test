import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
  @Input() okText: string = 'Save';
  @Input() cancelText: string = 'Cancel';
  @Input() placeholder: string = 'Enter text here';
  @Input() value: string = '';
  @Input() title: string = 'Dialog';
  @Input() showPrompt: boolean = false;

  @Output() valueEmitted  = new EventEmitter<string | null>;

  constructor() {
  }

  emitValue(value: string | null) {
    this.valueEmitted.emit(value);
   }
}
