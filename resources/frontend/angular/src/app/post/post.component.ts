import { Component } from '@angular/core';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent {
  posts: any;

  constructor(private postservice:PostsService) {}

  ngOnInit() {
    this.getPosts()
  }

  getPosts() {
    this.postservice.getAll()
      .then(response => {
        this.posts = response.data.posts;
      })
  }
}
