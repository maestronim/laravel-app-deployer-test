import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor() { }

  async getAll() {
    try {
      const resp = await axios.get(environment.apiUrl + "/todos");
      return resp.data;
    } catch(err) {
      console.log(err)
    }
  }

  async create(title: string) {
    try {
      const resp = await axios.post(environment.apiUrl + "/todos", {title: title});
      return resp.data;
    } catch(err) {
      console.log(err)
    }
  }

  async update(id: number, title: string, completed: boolean | null = null) {
    try {
      const resp = await axios.put(environment.apiUrl + "/todos/" + id, {title: title, completed: completed});
      return resp.data;
    } catch(err) {
      console.log(err)
    }
  }

  async delete(id: number) {
    try {
      const resp = await axios.delete(environment.apiUrl + "/todos/" + id);
      return resp.data;
    } catch(err) {
      console.log(err)
    }
  }
}
