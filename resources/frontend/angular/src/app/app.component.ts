import { Component } from '@angular/core';
import { Todo } from './todo';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'Todo list';
  fieldValue = '';
  todoList: Todo[] = [];
  editingTodo: Todo | null = null;
  okButtonText = 'Create';
  showDialog: boolean = false;
  dialogTitle: string = 'Create task'

  constructor(private todoservice: TodoService) {
  }

  ngOnInit() {
    this.fetchTodos();
  }

  async fetchTodos() {
    await this.todoservice.getAll().then((res) => {
      this.todoList = res.todos;
    });
  }

  todoDialog(todo: Todo | null = null) {
    this.dialogTitle = 'Create task';
    this.okButtonText = 'Create';
    this.fieldValue = '';
    this.editingTodo = todo;
    if (todo) {
      this.fieldValue = todo.title;
      this.dialogTitle = 'Edit task';
      this.okButtonText = 'Save';
    }
    this.showDialog = true;
  }

  async todoUpdated(title: string | null) {
    if (title) {
      title = title.trim();
      if (this.editingTodo) {
        await this.updateTodo(title);
      } else {
        await this.addTodo(title);
      }
    }
    this.showDialog = false;

    this.fetchTodos();
  }

  async addTodo(title: string) {
    await this.todoservice.create(title)
  }

  async updateTodo(title: string) {
    if (this.editingTodo) {
      await this.todoservice.update(this.editingTodo.id, title);
    }
  }

  async completedTodo(todo: Todo) {
    await this.todoservice.update(todo.id, todo.title, todo.completed);

    this.fetchTodos();
  }

  async removeTodo(id: number) {
    await this.todoservice.delete(id);

    this.fetchTodos();
  }
}
