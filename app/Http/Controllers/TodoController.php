<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $todos = Todo::get();

        return response()->json(compact('todos'), 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            "title" => "required|string"
        ]);

        Todo::create([
            "title" => $request->title
        ]);

        return response()->json("Todo created", 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            "title" => "required|string",
            "completed" => "nullable|boolean"
        ]);

        $todo = Todo::find($id);
        if(empty($todo)) {
            return response()->json("Todo not found", 404);
        }

        $todo->title = $request->title;
        $todo->completed = $request->completed ?? $todo->completed;
        $todo->save();

        return response()->json("Todo updated", 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $todo = Todo::find($id);
        if(empty($todo)) {
            return response()->json("Todo not found", 404);
        }

        $todo->delete();

        return response()->json("Todo removed", 200);
    }
}
